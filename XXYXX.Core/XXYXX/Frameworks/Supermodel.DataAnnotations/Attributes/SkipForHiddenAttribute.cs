﻿using System;

namespace Supermodel.DataAnnotations.Attributes;

public class SkipForHiddenAttribute : Attribute { }