﻿namespace WMDomain.Entities;

public enum PriorityEnum
{
    High,
    Medium,
    Low
}