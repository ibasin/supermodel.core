﻿using WebMonk.HttpRequestHandlers;

namespace HTML2RazorSharpWM.Mvc.Util;

public class DefaultPathRedirector() : DefaultPathRedirectorHttpRequestHandlerBase("/main/index");